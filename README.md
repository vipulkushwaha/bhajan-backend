# Bhajan Backend

This is a bhajan backend that makes use of standard templating as well as rest APIs.

## Installation 


pip install -r requirements.txt

## Running


python manage.py runserver


## Setup


python manage.py migrate


## Create Superuser


python manage.py createsuperuser